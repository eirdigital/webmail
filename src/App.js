import React, { Component } from "react";
import { hot } from "react-hot-loader";
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import "../assets/style/generic.scss";
import "../assets/style/header.scss";
import "../assets/style/app.scss";
import Login from "./components/login/Login";
import Header from "./components/generic/Header"
import Footer from "./components/generic/Footer"
import Dashboard from "./components/dashboard/Dashboard"
import Billing from "./components/billing/Billing"
import AccountManagement from './components/accountManagement/AccountManagement';
require('../assets/images/bg_eir_white_0.38_opacity.png');
require('../assets/images/eir_logo_pink.png');
require('../assets/fonts/GT-Walsheim-Light.woff');
require('../assets/fonts/eir-regular-web.woff');
require('../assets/fonts/GT-Walsheim-Regular.woff');
require('../assets/fonts/GT-Walsheim-Medium.woff');
const supportsHistory = 'pushstate' in window.history;
const getSessionValid = localStorage.getItem("myeir.session");
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      environment: "https://mytt01.eir.ie/",
      loginSuccess: false,

    };

    this.getLoginResponse = this.getLoginResponse.bind(this);
  }
  componentWillMount() {

    // this.setState({ loginSuccess: (getSessionValid != undefined && getSessionValid > 0) ? true : false })
  }
  getLoginResponse(response) {
    // alert("response len >>> ", response.length);
    this.setState({ loginSuccess: response });
    return response;
  }
  render() {
    console.log("logged in resposnes >>>>>>>>>> :: ", this.state.loginSuccess);
    return (
      <Router>
        <div className="">
          {/* {
            !this.state.loginSuccess &&
            <Login environment={this.state.environment} sendLoginResponse={this.getLoginResponse} />
          } */}
          <div>
          {/* --- comment for webmail existing login --- */}
            {/*  {this.state.loginSuccess && */}
              <div>
                <Header environment={this.state.environment} sendLoginResponse={this.getLoginResponse} />
                <Switch>
                  <Route exact path="/wm/"><Dashboard environment={this.state.environment} /></Route>
                  <Route exact path="/wm/offer/"><Dashboard environment={this.state.environment} /></Route>
                  <Route exact path="/wm/logout/"><Login environment={this.state.environment} sendLoginResponse={this.getLoginResponse} /></Route>
                  <Route exact path="/wm/payment/"><Billing environment={this.state.environment} /></Route>
                  <Route exact path="/wm/accountManagement/"><AccountManagement environment={this.state.environment} /></Route>
                </Switch>
              </div>
            {/* } */}
            {/* --- comment for webmail existing login --- */}
            <Footer environment={this.state.environment} />
          </div>

        </div>
      </Router>
    );
  }
}
export default App;
