import React, { Component } from 'react'
import EditForm from '../editForm/EditForm';

class AccountManagement extends Component {
    handleSave = (value) => {
        console.log(value)
    }
    render() {
        return (
            <div>
                <div>Email Address: vivek.kumar@eir.ie</div>
                <div>Name: Vivek Kumar</div>
                <div>Mobile Number: <EditForm handleSave={this.handleSave} formValue="9112584" type='tel' placeholder='Mobile Number' maxLength='11' /></div>
                <div>Order details: Feb 5 2020</div>
                <div>Payment History: Feb 5 2020 £200</div>
                <div><a href="https://autoreg.eir.ie/passwordchange/changeknown_pwd.html">Change Password</a></div>
                <div><a href="https://autoreg.eir.ie/passwordchange/changeunknown_pwd.html">Reset Password</a></div>
            </div>
        )
    }
}

export default AccountManagement;
