import React, { Component } from "react";
import axios from 'axios';

class Login extends Component {
    // add a constructor to the class to initialize the states
    constructor(props) {
        super(props);

        this.userID = React.createRef();
        this.userPwd = React.createRef();
        this.state = {
            environment: this.props.environment,
            loginStatus: [],
            userDetails: [],
            loginSuccess: false,
            showError:false
        };

        this.handleLogin = this.handleLogin.bind(this);
        this.loginSuccess = this.loginSuccess.bind(this);

    }
    componentDidMount() {
        this.props.sendLoginResponse(false);
        this.setState({ loginSuccess: false })
    }
    handleLogin() {
        axios.post((this.state.environment) + `rest/brand/1/portalUser/authenticate`, { "emailAddress": this.userID.current.value, "password": this.userPwd.current.value })
            .then(res => {
                console.log("res >>>>>>>>>>>>>>>>>>>", res.data.statusCode);
                console.log("res >>>>>>>>>>>>>>>>>>>", res);
                console.log("loginSuccess >>>>>>>>>>>> ", this.state.loginSuccess);
                this.setState({ loginSuccess: true });
                console.log(res.data);

                this.setState({ loginStatus: "Details : " + res.data.data.uid });
                this.props.sendLoginResponse(true);
                // location.href = "/wm/dashboard/";
            })
            .catch(error => {
                this.setState({ userDetails: "" })
                let errorText = "Error occured"
                switch (error.response.status) {
                    case 400:
                        errorText = "Empty username / pwd ";
                        this.setState({showError:true})
                        break;
                    case 401:
                        errorText = "Invalid username / pwd ";
                        this.setState({showError:true})
                        break;
                    default:
                        errorText = errorText;
                }
                this.setState({ loginStatus: errorText })
                this.props.sendLoginResponse(this.state.loginSuccess);
                // localStorage.removeItem("myeir.session")
            });
    }



    //secure/brand/${brand}/portalUser/fixedAccounts
    loginSuccess() {
        console.log("this.state.loginSuccess >>>>>>> ", this.state.loginSuccess);
        // (this.state.loginSuccess) ? location.href = "/wm/dashboard" : location.href = "/webmail";
    }

    render() {
        return (
            <div>
                <div className="wrapper eir-nav__wrapper eir-nav__wholesale-tertiary">
                    <div className="row">
                        <a href="https://www.eir.ie" title="eir home"><div className="eir-nav__logo"></div></a>
                    </div>
                    <div className="wrapper eir-nav__stickynav-wrapper">
                        <div className="row eir-nav__stickynav-row">
                            <div className="eir-nav__stickynav large-12 columns">
                                <ul>
                                    <li className="eir-nav__left" style={{ "paddingRight": "30px", "marginLeft": "0" }}><a id="topnav-home-track" href="/">eir home</a></li>
                                    <li className="eir-nav__left"><a id="topnav-business-track" href="http://business.eir.ie/">eir business</a></li>
                                    <li><a id="topnav-myeir-track" href="https://my.eir.ie/eir/transactional/login">my eir</a></li>
                                    <li><a id="topnav-webmail-track" href="/email/">webmail</a></li>
                                    <li><a id="topnav-survey-track" href="https://eir.eu.qualtrics.com/jfe/form/SV_cPhepUrSwCgLcjP" target="_blank">feedback</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="wrapper eir-nav__midnav-wrapper">
                        <div className="row">
                            <div className="eir-nav__midnav large-12 columns">
                                <ul className="eir-nav-links">
                                    <li className=" "><a id="mainnav-broadband-track" href="/broadband/" className="eir-mob-toggle-slide">Broadband</a></li>
                                    <li className=" "><a id="mainnav-mobile-track" href="/mobile/" className="eir-mob-toggle-slide">Mobile</a></li>
                                    <li className=" "><a id="mainnav-tv-track" href="/tv/" className="eir-mob-toggle-slide">TV</a></li>
                                    <li><a id="eir-header-mainnav-sport-track" href="/sport/" className="eir-mob-toggle-slide">Sport</a></li>
                                    <li className=" "><a id="mainnav-support-track" href="/support" className=" eir-mob-toggle-slide">Need some help?</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            <div className="webmail-login__container">
                <h2>Webmail Login</h2>
                <form id="login-form-email">
                    <div className="form-input">
                        <label htmlFor="emailAddress" className="white">Email address</label>
                        <input className="webmail-login__text-input" id="userID" ref={this.userID} />
                        <label htmlFor="emailPassword" className="white">Password</label>
                        <input className="webmail-login__text-input" type="password" id="userPwd" ref={this.userPwd} />
                        <div className="errorMessage">
                            {
                                this.state.showError && 
                                <span>Invalid email / password</span>
                            }
                            {/* <div>This field is required</div>
                            <div>Invalid email</div> */}
                        </div>
                    </div>

                    <div className="login-page-links">
                        <a target="_blank" href="" className="login-page-link">Forgot your email?</a>
                        <a target="_blank" href="https://autoreg.eir.ie/passwordchange/changeunknown_pwd.html" className="login-page-link">Forgot or change your password?</a>
                        <a target="_blank" href="" className="login-page-link">Don't have an account? Register</a>
                    </div>
                    <button className={"btn btn--medium btn--green"} type="button" onClick={this.handleLogin}>Login to webmail</button>
                </form>
            </div>
        </div>
        )
    }
}

export default Login;