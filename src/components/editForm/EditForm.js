import React, { Component } from 'react'

class EditForm extends Component {
    constructor(props) {
        super(props)

        this.textInput = React.createRef();
        this.state = {
            fieldDisabled: true,
            value: '',
            showEdit: true,
            showSave: false,
            std: '',
            stds: [
                "01",
                "021",
                "022",
                "023",
                "024",
                "025",
                "026",
                "027",
                "028",
                "029",
                "0402",
                "0404",
                "041",
                "042",
                "043",
                "044",
                "045",
                "046",
                "047",
                "049",
                "0504",
                "0505",
                "051",
                "052",
                "053",
                "056",
                "057",
                "058",
                "059",
                "061",
                "062",
                "063",
                "064",
                "065",
                "066",
                "067",
                "068",
                "069",
                "071",
                "074",
                "090",
                "091",
                "093",
                "094",
                "095",
                "096",
                "097",
                "098",
                "099",
                "8881",
                "8882",
                "8883",
            ],
        }
    }

    componentDidMount() {
        this.setState({
            value: this.props.formValue ? this.props.formValue : '',
            std: '01'
        })
    }


    handleEdit = (event) => {
        event.preventDefault();
        this.setState({
            fieldDisabled: false,
            showSave: true,
            showEdit: false
        })

        setTimeout(() => {
            this.textInput.current.focus();
        }, 0)
    }

    handleCancel = (event) => {
        event.preventDefault();
        this.setState({
            fieldDisabled: true,
            showEdit: true,
            showSave: false,
            std: '01',
            value: this.props.formValue ? this.props.formValue : ''
        })
    }

    handleSave = (event) => {
        event.preventDefault();
        this.setState({
            fieldDisabled: true,
            showEdit: true,
            showSave: false
        })
        let number;
        if(this.props.type === 'tel' || this.props.type === 'number') {
            number = this.state.std.toString() + this.state.value.toString()
        } else {
            number = this.state.value
        }
        if (this.props.handleSave) {
            this.props.handleSave(number)
        }        
    }

    handleClick = (event) => {
        if (event.target.validity.valid) {
            this.setState({
                value: event.target.value
            })
        }
    }

    handleStd = (event) => {
        this.setState({
            std: event.target.value
        })
    }


    render() {
        const { fieldDisabled, value, showEdit, showSave, std } = this.state;
        const { type, maxLength, pattern, placeholder } = this.props
        return (
            <div>
                <form>
                    <label>
                        {(type === 'tel' || type === 'number') &&
                        <select 
                            disabled={fieldDisabled} 
                            value={std} 
                            onChange={this.handleStd}>
                            {this.state.stds.map((stds, i) => <option key={i}>{stds}</option>)}
                        </select>
                        }
                    </label>
                    <label>
                    <input
                        value={value}
                        ref={this.textInput}
                        placeholder={placeholder}
                        onChange={this.handleClick}
                        disabled={fieldDisabled}
                        type={type === 'text' || type === 'number' ? 'text' : type}
                        name="name"
                        pattern={pattern}
                        // pass regular expression pattern="^[0-9]*$" as props for number/tel type input to get only numbers
                        maxLength={maxLength}
                    />
                    </label>
                    {showEdit &&
                        <button onClick={this.handleEdit}>Edit</button>
                    }
                    {showSave &&
                        <span>
                            <button onClick={this.handleCancel}>Cancel</button>
                            <button onClick={this.handleSave}>Save</button>
                        </span>
                    }
                </form>
            </div>
        )
    }
}

export default EditForm;


// *pass regular expression pattern="^[0-9]*$" as props for number/tel type input to get only numbers

// Props to be used:
// 1. handleSave()
// 2. formValue
// 3. type
// 4. pattern *
// 5. maxLength
// 6. placeholder