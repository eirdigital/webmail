import React, { Component } from "react";
import axios from 'axios';

class Footer extends Component {
    // add a constructor to the class to initialize the states
    constructor(props) {
        super(props);
        this.state = {
            environment: this.props.environment,
        };



    }
   

    render() {
        return (
            <div className="footer">
                {/* <h1>&lt;Footer&gt;</h1> */}
                <p>
               © 2020 eir. All rights reserved.
                </p>
            </div>
        );
    }
}

export default Footer;