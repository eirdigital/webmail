import React, { Component } from "react";
import axios from 'axios';
// import { Link } from 'react-router-dom';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
class Header extends Component {
    // add a constructor to the class to initialize the states
    constructor(props) {
        super(props);
        this.state = {
            environment: this.props.environment,
        };



    }
    goToLogin() {
        this.props.sendLoginResponse(false);
        localStorage.removeItem("myeir.session")
        location.href="/webmail";
        window.location.reload();
    }

    render() {
        return (
            <div className="App">
                    <Link to="/wm/offer/" >Offer</Link> | 
                    <Link to="/wm/payment/" > Payment</Link> | 
                    <Link to="/wm/accountManagement/" > Account Management</Link> |
                    <a href="/wm/" onClick={this.goToLogin.bind(this)}>Logout</a>
                {/* </ul> */}
            </div>
        );
    }
}

export default Header;