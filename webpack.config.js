const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const client = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'bundle.js',
    publicPath: '/',
  },

  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader',
        }),
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
            use: ["css-loader"],
            // use style-loader in development
            fallback: "style-loader"
        })
      },
      {
        test: /\.(png|jpg|gif|mp3)/,
        loader: 'file-loader',
        options: {
          name: 'images/[name].[ext]',
        },
      },
      {
  test: /\.svg$/,
  loader: 'file-loader',
      options: {
        name: 'svg/[name].[ext]',
      },

},
      {
      test: /\.(ttf|eot|woff|woff2)$/,
      loader: 'file-loader',
      options: {
        name: 'fonts/[name].[ext]',
      },
      }
    ],
  },
  
  devServer: {
    historyApiFallback: true,
    contentBase:  path.join(__dirname, "public/"),
    port: process.env.PORT,
    publicPath: "http://localhost:4000/wm/dist/",
    hotOnly: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin('styles.css'),
  ],
}
module.exports = client;